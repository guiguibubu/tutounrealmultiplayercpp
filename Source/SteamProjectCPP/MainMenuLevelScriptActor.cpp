// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuLevelScriptActor.h"
#include "Kismet/GameplayStatics.h"
#include "Network/GameInfoInstance.h"

void AMainMenuLevelScriptActor::BeginPlay() {
	TWeakObjectPtr<UWorld> world = GetWorld();
	if (!ensure(world.IsValid())) return;
	TWeakObjectPtr<UGameInstance> gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	if (!ensure(gameInstance.IsValid())) return;
	TWeakObjectPtr<UGameInfoInstance> gameInstanceRef = Cast<UGameInfoInstance>(gameInstance);
	if (!ensure(gameInstanceRef.IsValid())) return;
	gameInstanceRef->showMainMenuWidget(world->GetFirstPlayerController());
}
