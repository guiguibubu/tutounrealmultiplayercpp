// Fill out your copyright notice in the Description page of Project Settings.

#include "GameplayGM.h"
#include "GameplayPC.h"
#include "Network/GameInfoInstance.h"
#include "Network/MyPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

#include "Engine/Engine.h"

AGameplayGM::AGameplayGM(const FObjectInitializer & ObjectInitializer) : AGameMode(ObjectInitializer)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	PlayerControllerClass = AGameplayPC::StaticClass();
	PlayerStateClass = AMyPlayerState::StaticClass();

	//gameInstanceRef = Cast<UGameInfoInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	//if (gameInstanceRef.IsValid()) {
	//	gameInstanceRef->cleanWidget();
	//}
}

void AGameplayGM::PostLogin(APlayerController * NewPlayer) {
	Super::PostLogin(NewPlayer);

	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode AGameplayGM Login")));
	TWeakObjectPtr<AGameplayPC> playerController = Cast<AGameplayPC>(NewPlayer);
	if (playerController.IsValid()) {
		AllPlayerControllers.Add(playerController.Get());
	}
}

void AGameplayGM::Logout(AController* Exiting) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode AGameplayGM Logout")));
	TWeakObjectPtr<UGameInfoInstance> gameInstanceRef = Cast<UGameInfoInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!ensure(gameInstanceRef.IsValid())) return;
	gameInstanceRef->DestroySession();

	TWeakObjectPtr<AGameplayPC> playerController = Cast<AGameplayPC>(Exiting);
	//if (IsValid(playerController) && playerController->IsLocalController()) {
	//	gameInstanceRef->mainMenu(Cast<APlayerController>(Exiting));
	//}
	//if (Exiting->HasAuthority()) {
	//	gameInstanceRef->DestroySession(); 
	//}
	if (playerController.IsValid() && playerController->IsLocalController()) {
		playerController->DestroySession();
	}
}

void AGameplayGM::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode AGameplayGM EndPlay")));
	if (!EndPlayReason == EEndPlayReason::LevelTransition) {
		if (HasAuthority()) {
			//logoutAllClients();
		}
	}
}

void AGameplayGM::RespawnPlayer_Implementation(AGameplayPC* playerController, TSubclassOf<ACharacter> MyPlayerCharacter) {
	TWeakObjectPtr<APawn> pawn = playerController->GetPawn();
	FTransform position{};
	if (pawn.IsValid()) {
		position = pawn->GetActorTransform();
		pawn->Destroy();
	}
	TWeakObjectPtr<UWorld> world = GetWorld();
	pawn = world->SpawnActor<ACharacter>(MyPlayerCharacter, position, {});
	playerController->Possess(pawn.Get());
}

bool AGameplayGM::RespawnPlayer_Validate(AGameplayPC* playerController, TSubclassOf<ACharacter> MyPlayerCharacter) {
	return true;
}