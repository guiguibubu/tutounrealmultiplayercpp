// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "GameplayGM.generated.h"

/**
 * 
 */
UCLASS(MinimalAPI)
class AGameplayGM : public AGameMode
{
	GENERATED_BODY()
	
	TArray<class AGameplayPC*> AllPlayerControllers;

public:
	AGameplayGM(const FObjectInitializer & ObjectInitializer);
	
	void PostLogin(APlayerController * NewPlayer) override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void Logout(AController* Exiting) override;

	UFUNCTION(Reliable, Server, WithValidation)
	void RespawnPlayer(AGameplayPC* playerController, TSubclassOf<ACharacter> MyPlayerCharacter);

	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;
};
