// Fill out your copyright notice in the Description page of Project Settings.

#include "GameplayPC.h"

#include "Kismet/GameplayStatics.h"

#include "Network/GameInfoInstance.h"
#include "Network/MyPlayerState.h"
#include "GameplayGM.h"

#include "Engine/Engine.h"

AGameplayPC::AGameplayPC() {
	gameInstanceRef = Cast<UGameInfoInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
}

void AGameplayPC::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	Super::EndPlay(EndPlayReason);
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("PlayerController Logout")));
	if (EndPlayReason != EEndPlayReason::LevelTransition && EndPlayReason != EEndPlayReason::Destroyed) {
		this->DestroySession();
	}
}

void AGameplayPC::BeginPlay() {
	Super::BeginPlay();
	CleanupGameViewport();
	InitPawn();
}

void AGameplayPC::InitPawn_Implementation() {
	GetWorld()->GetAuthGameMode<AGameplayGM>()->RespawnPlayer(this, this->GetPlayerState<AMyPlayerState>()->S_PlayerInfo.MyPlayerCharacter);
}

bool AGameplayPC::InitPawn_Validate() {
	return true;
}

void AGameplayPC::DestroySession_Implementation() {
	gameInstanceRef->DestroySession();
}

void AGameplayPC::cleanWidget_Implementation() {
	gameInstanceRef->cleanWidget();
}