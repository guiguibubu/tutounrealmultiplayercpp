// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GameplayPC.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPROJECTCPP_API AGameplayPC : public APlayerController
{
	GENERATED_BODY()
	
public:
	AGameplayPC();

	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void BeginPlay() override;

	UFUNCTION(Reliable, Server, WithValidation)
	void InitPawn();

	UFUNCTION(Reliable, Client)
	void DestroySession();

	UFUNCTION(Reliable, Client)
	void cleanWidget();
};
