// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyGM.h"
#include "LobbyPC.h"
#include "Network/GameInfoInstance.h"
#include "Network/MyPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "LobbyPC.h"
#include "Engine/Engine.h"

ALobbyGM::ALobbyGM(const FObjectInitializer & ObjectInitializer) : AGameMode(ObjectInitializer)
{
	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	//if (PlayerPawnBPClass.Class != NULL)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}
	PlayerControllerClass = ALobbyPC::StaticClass();
	PlayerStateClass = AMyPlayerState::StaticClass();

	bUseSeamlessTravel = true;

	int nbCharacter = 6;
	for (int i = 0; i < nbCharacter; ++i) {
		AvailableCharacters.Add(true);
	}

	ConstructorHelpers::FClassFinder<ACharacter> PlayerPawn0_Class(TEXT("/Game/BluePrints/Characters/0_Base"));
	if (!ensure(IsValid(PlayerPawn0_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> PlayerPawn1_Class(TEXT("/Game/BluePrints/Characters/1_Base"));
	if (!ensure(IsValid(PlayerPawn1_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> PlayerPawn2_Class(TEXT("/Game/BluePrints/Characters/2_Base"));
	if (!ensure(IsValid(PlayerPawn2_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> PlayerPawn3_Class(TEXT("/Game/BluePrints/Characters/3_Base"));
	if (!ensure(IsValid(PlayerPawn3_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> PlayerPawn4_Class(TEXT("/Game/BluePrints/Characters/4_Base"));
	if (!ensure(IsValid(PlayerPawn4_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> PlayerPawn5_Class(TEXT("/Game/BluePrints/Characters/5_Base"));
	if (!ensure(IsValid(PlayerPawn5_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> PlayerPawn6_Class(TEXT("/Game/BluePrints/Characters/6_Base"));
	if (!ensure(IsValid(PlayerPawn6_Class.Class))) return;

	Characters.Reserve(nbCharacter);
	Characters.Add(MoveTemp(PlayerPawn0_Class.Class));
	Characters.Add(MoveTemp(PlayerPawn1_Class.Class));
	Characters.Add(MoveTemp(PlayerPawn2_Class.Class));
	Characters.Add(MoveTemp(PlayerPawn3_Class.Class));
	Characters.Add(MoveTemp(PlayerPawn4_Class.Class));
	Characters.Add(MoveTemp(PlayerPawn5_Class.Class));
	Characters.Add(MoveTemp(PlayerPawn6_Class.Class));

	DefaultPawnClass = Characters[0];
}

void ALobbyGM::PostLogin(APlayerController * NewPlayer) {
	Super::PostLogin(NewPlayer);
	TWeakObjectPtr<ALobbyPC> playerController = Cast<ALobbyPC>(NewPlayer);
	if (playerController.IsValid()) {
		AllPlayerControllers.Add(playerController.Get());
		PlayerReady.Add(false);

		FPlayerInfo* playerSettings = &playerController->GetPlayerState<AMyPlayerState>()->S_PlayerInfo;
		playerSettings->MyPlayerCharacter = Characters[0];
		playerSettings->MyPlayerStatus = false;

		CurrentPlayers++;
		
		//playerController->AssignSelectedCharacter(0);
		//SwapCharacter(playerController.Get(), Characters[0], false);
		playerController->showLobbyMenuWidget(true, EInputModeWidget::GameAndUI);
	}

	if (!MaxPlayer) {
		MaxPlayer = GetGameInstance<UGameInfoInstance>()->nbPlayer_;
	}

	EveryoneUpdate();
}

void ALobbyGM::Logout(AController* Exiting) {
	Super::Logout(Exiting);
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode ALobbyGM Logout")));
	TWeakObjectPtr<ALobbyPC> playerController = Cast<ALobbyPC>(Exiting);
	int index;
	if (AllPlayerControllers.Find(playerController.Get(), index)) {
		AllPlayerControllers.RemoveAt(index);
		PlayerReady.RemoveAt(index);
		AllConnectedPlayers.RemoveAt(index);
		CurrentPlayers--;
		EveryoneUpdate();
	}
}

void ALobbyGM::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode ALobbyGM EndPlay")));
	if (!EndPlayReason == EEndPlayReason::LevelTransition) {
		if (HasAuthority()) {
			logoutAllClients();
		}
	}
}

void ALobbyGM::StartGame_Implementation() {
	TWeakObjectPtr<UWorld> world = GetWorld();
	if (!ensure(world.IsValid())) return;
	//if (world->IsServer()) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Start game"));
	for (int i = 0; i < CurrentPlayers; ++i) {
		TWeakObjectPtr<ALobbyPC> playerController = AllPlayerControllers[i];
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode ALobbyGM start game %s"), *playerController->GetFName().ToString()));
		playerController->cleanWidget();
		playerController->showLoadingScreenWidget();
	}
	world->ServerTravel("/Game/Maps/Arena", false, false);
	//}
	//else {
	//	//gameInstanceRef->showErrorWidget();
	//}
}

bool ALobbyGM::StartGame_Validate()
{
	return true;
}

void ALobbyGM::logoutAllClients_Implementation()
{
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("logoutClients_Implementation")));
	for (int i = 0; i < CurrentPlayers; ++i) {
		AllPlayerControllers[i]->DestroySession();
	}
}

bool ALobbyGM::logoutAllClients_Validate()
{
	return true;
}

void ALobbyGM::SwapCharacter_Implementation(ALobbyPC* playerController, TSubclassOf<ACharacter> MyPlayerCharacter, const bool changedStatusIn) {
	if (!changedStatusIn) {
		TWeakObjectPtr<APawn> pawn = playerController->GetPawn();
		FTransform position{};
		if (pawn.IsValid()) {
			position = pawn->GetActorTransform();
			pawn->Destroy();
		}
		TWeakObjectPtr<UWorld> world = GetWorld();
		pawn = world->SpawnActor<ACharacter>(MyPlayerCharacter, position, {});
		playerController->Possess(pawn.Get());
	}
}

bool ALobbyGM::SwapCharacter_Validate(ALobbyPC* playerController, TSubclassOf<ACharacter> MyPlayerCharacter, const bool changedStatusIn)
{
	return true;
}

void ALobbyGM::EveryoneUpdate_Implementation() {
	AllConnectedPlayers.Empty();
	for (ALobbyPC* playerController : AllPlayerControllers) {
		AllConnectedPlayers.Add(playerController->GetPlayerState<AMyPlayerState>()->S_PlayerInfo);
		playerController->UpdateNumberOfPlayers(CurrentPlayers, MaxPlayer);
	}
	for (ALobbyPC* playerController : AllPlayerControllers) {
		playerController->AddPlayerInfo(AllConnectedPlayers);
		playerController->UpdateAvailableCharacters(AvailableCharacters);
	}
	canWeStart = true;
	int i = 0;
	while(canWeStart && i<CurrentPlayers) {
		canWeStart = AllConnectedPlayers[i].MyPlayerCharacter != Characters[0];
		++i;
	}
}

bool ALobbyGM::EveryoneUpdate_Validate()
{
	return true;
}

void ALobbyGM::UpdateAvailableCharacter(uint16 SelectedCharacterID, uint16 PreviousCharacterID) {
	if (SelectedCharacterID > 0) {
		if (AvailableCharacters[SelectedCharacterID-1]) {
			AvailableCharacters[SelectedCharacterID-1] = false;
		}
	}
	if (PreviousCharacterID > 0) {
		AvailableCharacters[PreviousCharacterID - 1] = true;
	}
}
