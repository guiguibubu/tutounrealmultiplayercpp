// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/PlayerStart.h"
#include "GameFramework/Character.h"
#include "Network/PlayerInfo.h"
#include "LobbyGM.generated.h"

/**
 * 
 */
UCLASS(MinimalAPI)
class ALobbyGM : public AGameMode
{
	GENERATED_BODY()

public:
	ALobbyGM(const FObjectInitializer & ObjectInitializer);

	void PostLogin(APlayerController * NewPlayer) override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void Logout(AController* Exiting) override;

	UFUNCTION(Reliable, Server, WithValidation)
	void logoutAllClients();

	UFUNCTION(Reliable, Server, WithValidation)
	void StartGame();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	bool canWeStart = false;
	UPROPERTY(EditAnywhere, Category = "ServerSettings")
	TArray<ALobbyPC*> AllPlayerControllers;
	UPROPERTY(EditAnywhere, Category = "ServerSettings")
	TArray<bool> PlayerReady;
	UPROPERTY(EditAnywhere, Category = "ServerSettings")
	TArray<APlayerStart*> SpawnPoints;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	FText ServerName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	int MaxPlayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	int CurrentPlayers;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	TArray<FPlayerInfo> AllConnectedPlayers;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	TArray<bool> AvailableCharacters;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	TArray<TSubclassOf<ACharacter>> Characters;

	// ----------------------------UPDATE CHARACTER ----------------------
	UFUNCTION(Reliable, Server, WithValidation)
	void SwapCharacter(ALobbyPC* playerController, TSubclassOf<ACharacter> MyPlayerCharacter, const bool changedStatusIn);
	UFUNCTION(Reliable, Server, WithValidation)
	void EveryoneUpdate();
	UFUNCTION()
	void UpdateAvailableCharacter(uint16 SelectedCharacterID, uint16 PreviousCharacterID);
};
