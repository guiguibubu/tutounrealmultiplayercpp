// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyPC.h"

#include "Kismet/GameplayStatics.h"
#include "Network/GameInfoInstance.h"
#include "UI/LobbyMenuWidget.h"
#include "UI/ErrorWidget.h"
#include "UI/LoadingScreenWidget.h"
#include "Network/Lobby/LobbyGM.h"
#include "GameFramework/PlayerState.h"

#include "Network/MyPlayerState.h"

#include "Engine/Engine.h"

ALobbyPC::ALobbyPC(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	if (GetWorld()) {
		gameInstanceRef = GetGameInstance<UGameInfoInstance>();
	}
}

void ALobbyPC::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	Super::EndPlay(EndPlayReason);
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("PlayerController ALobbyPC EndPlay")));
	if (EndPlayReason != EEndPlayReason::LevelTransition && EndPlayReason != EEndPlayReason::Destroyed) {
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("PlayerController ALobbyPC EndPlay DestroySession")));
		this->DestroySession();
	}
	cleanWidget();
}

void ALobbyPC::logoutAllClients_Implementation()
{
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("logoutClients_Implementation")));
	this->DestroySession();
}

void ALobbyPC::DestroySession_Implementation() {
	UGameInfoInstance* gameInstance = GetGameInstance<UGameInfoInstance>();
	if (gameInstance) {
		showLoadingScreenWidget();
		gameInstance->DestroySession(this); 
	}
}

void ALobbyPC::showLoadingScreenWidget_Implementation(const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!LoadingScreenWB.IsValid()) {
		LoadingScreenWB = Cast<ULoadingScreenWidget>(gameInstanceRef->createWidget(this, gameInstanceRef->LoadingScreenWB_Class));
	}
	TWeakObjectPtr<UMenuWidget> widgetCast = Cast<UMenuWidget>(LoadingScreenWB);
	gameInstanceRef->showWidget(widgetCast, gameInstanceRef->LoadingScreenWB_Class, this, bShowMouseCursor, inputMode);
}

void ALobbyPC::showErrorWidget_Implementation(const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	gameInstanceRef->showErrorWidget(this, bShowMouseCursor, inputMode);
}

void ALobbyPC::showLobbyMenuWidget_Implementation(const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("showLobbyMenuWidget_Implementation %s"), *this->GetFName().ToString()));
	if (!LobbyMenuWD.IsValid()) {
		LobbyMenuWD = Cast<ULobbyMenuWidget>(gameInstanceRef->createWidget(this, gameInstanceRef->LobbyMenuWD_Class));
	}
	TWeakObjectPtr<UMenuWidget> widgetCast = Cast<UMenuWidget>(LobbyMenuWD);
	gameInstanceRef->showWidget(widgetCast, gameInstanceRef->LobbyMenuWD_Class, this, bShowMouseCursor, inputMode);
}

void ALobbyPC::cleanWidget() {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("cleanWidget_Implementation %s"), *this->GetFName().ToString()));
	if (LobbyMenuWD.IsValid()) {
		LobbyMenuWD->Clean(true);
	}
	if (LoadingScreenWB.IsValid()) {
		LoadingScreenWB->Clean(true);
	}
}

void ALobbyPC::AssignPlayer(TSubclassOf<ACharacter> character, UTexture2D* image) {
	FPlayerInfo* playerSettings = &GetPlayerState<AMyPlayerState>()->S_PlayerInfo;
	playerSettings->MyPlayerCharacter = character;
	playerSettings->MyPlayerCharacterImage = image;
	Update(false);
}

//void ALobbyPC::AssignPlayer_Implementation(TSubclassOf<ACharacter> character, UTexture2D* image) {
//	FPlayerInfo* playerSettings = &GetPlayerState<AMyPlayerState>()->S_PlayerInfo;
//	playerSettings->MyPlayerCharacter = character;
//	playerSettings->MyPlayerCharacterImage = image;
//	Update(*playerSettings, false);
//}

//bool ALobbyPC::AssignPlayer_Validate(TSubclassOf<ACharacter> character, UTexture2D* image) {
//	return true;
//}

void ALobbyPC::AssignSelectedCharacter_Implementation(int16 characterId, UTexture2D* image) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Character Selected %d"), characterId));
	PreviousCharacterID = SelectedCharacterID;
	SelectedCharacterID = characterId;
	CharacterImage = image;
	CheckCharacter();
}

bool ALobbyPC::AssignSelectedCharacter_Validate(int16 characterId, UTexture2D* image) {
	return true;
}

void ALobbyPC::Update_Implementation(const bool changedStatusIn) {
	FPlayerInfo* playerSettings = &GetPlayerState<AMyPlayerState>()->S_PlayerInfo;
	TWeakObjectPtr<UWorld> world = GetWorld();
	TWeakObjectPtr<ALobbyGM> gameMode = Cast<ALobbyGM>(world->GetAuthGameMode());
	gameMode->SwapCharacter(this, playerSettings->MyPlayerCharacter, changedStatusIn);
	gameMode->EveryoneUpdate();
}

bool ALobbyPC::Update_Validate(const bool changedStatusIn) {
	return true;
}

void ALobbyPC::UpdateNumberOfPlayers_Implementation(const uint16& currentPlayers, const uint16& maxPlayers) {
}

void ALobbyPC::AddPlayerInfo_Implementation(const TArray<FPlayerInfo>& connectedPlayers) {
	AllConnectedPlayers = connectedPlayers;
}

void ALobbyPC::UpdateAvailableCharacters_Implementation(const TArray<bool>& availableCharacters) {
	AvailableCharacters = TArray<bool>(availableCharacters);
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("UpdateAvailableCharacters_Implementation %s"), *GetFName().ToString()));
	for (int i = 0; i < AvailableCharacters.Num(); ++i) {
		if (LobbyMenuWD.IsValid()) {
			//LobbyMenuWD->CharacterSelectWD->CharacterPanel->characterSelectButtons[i]->SetIsEnabled(AvailableCharacters[i]);
			TWeakObjectPtr<UCharacterSelectWidget> characterSelect = LobbyMenuWD->CharacterSelectWD;
			TWeakObjectPtr<UCharacterSelectButtonPoolWidget> buttonPool = characterSelect->CharacterPanel;
			TWeakObjectPtr<UCharacterSelectButton> button = buttonPool->characterSelectButtons[i];
			button->SetIsEnabled(AvailableCharacters[i]);
		}
	}
}

void ALobbyPC::CheckCharacter() {
	TWeakObjectPtr<UWorld> world = GetWorld();
	TWeakObjectPtr<ALobbyGM> gameMode = Cast<ALobbyGM>(world->GetAuthGameMode());
	gameMode->UpdateAvailableCharacter(SelectedCharacterID, PreviousCharacterID);
	AssignPlayer(gameMode->Characters[SelectedCharacterID], CharacterImage.Get());
}