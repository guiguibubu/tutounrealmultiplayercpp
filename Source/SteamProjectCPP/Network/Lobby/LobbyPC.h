// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Network/PlayerSaveGame.h"
#include "Engine/Texture2D.h"
#include "UI/MenuWidget.h"
#include "Network/PlayerInfo.h"
#include "LobbyPC.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class STEAMPROJECTCPP_API ALobbyPC : public APlayerController
{
	GENERATED_BODY()
public:
	ALobbyPC(const FObjectInitializer & ObjectInitializer);

	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	//-----------------------SESSION-----------------------------
	UFUNCTION(Reliable, Client)
	void logoutAllClients();

	UFUNCTION(Reliable, Client)
	void DestroySession();

	//--------------------------WIDGET-----------------------------
	UPROPERTY(EditAnywhere, Category = "Widget")
	TWeakObjectPtr<class UErrorWidget> ErrorWD;

	UPROPERTY(EditAnywhere, Category = "Widget")
	TWeakObjectPtr<class ULoadingScreenWidget> LoadingScreenWB;

	UPROPERTY(EditAnywhere, Category = "Widget")
	TWeakObjectPtr<class ULobbyMenuWidget> LobbyMenuWD;

	UFUNCTION(Reliable, Client)
	void showLoadingScreenWidget(const bool bShowMouseCursorIn = false, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);
	UFUNCTION(Reliable, Client)
	void showErrorWidget(const bool bShowMouseCursorIn = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);
	UFUNCTION(Reliable, Client)
	void showLobbyMenuWidget(const bool bShowMouseCursorIn = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);
	//UFUNCTION(Reliable, Client)
	void cleanWidget();

	// ----------------------------UPDATE CHARACTER ----------------------

	uint16 SelectedCharacterID;
	uint16 PreviousCharacterID;
	TArray<FPlayerInfo> AllConnectedPlayers;
	TArray<bool> AvailableCharacters;
	TWeakObjectPtr<UTexture2D> CharacterImage;

	UFUNCTION(Reliable, Server, WithValidation)
	void AssignSelectedCharacter(int16 characterId, UTexture2D* image = nullptr);
	//UFUNCTION(Reliable, Client)
	void AssignPlayer(TSubclassOf<ACharacter> characterClass, UTexture2D* image = nullptr);
	UFUNCTION(Reliable, Server, WithValidation)
	void Update(const bool changedStatusIn);
	UFUNCTION(Reliable, Client)
	void UpdateNumberOfPlayers(const uint16& currentPlayers, const uint16& maxPlayers);
	UFUNCTION(Reliable, Client)
	void AddPlayerInfo(const TArray<FPlayerInfo>& connectedPlayers);
	UFUNCTION(Reliable, Client)
	void UpdateAvailableCharacters(const TArray<bool>& availableCharacters);

	void CheckCharacter();
};
