// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPlayerState.h"
#include "UnrealNetwork.h"

#include "Engine/Engine.h"

AMyPlayerState::AMyPlayerState(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	bReplicates = true;
}

void AMyPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMyPlayerState, S_PlayerInfo);
}

void AMyPlayerState::CopyProperties(APlayerState* PlayerState) {
	Super::CopyProperties(PlayerState);
	if (PlayerState) {
		AMyPlayerState* TestPlayerState = Cast<AMyPlayerState>(PlayerState);
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("CopyProperties"), *TestPlayerState->GetFName().ToString()));
		if (TestPlayerState) {
			TestPlayerState->S_PlayerInfo = S_PlayerInfo;
		}
	}
}

void AMyPlayerState::OverrideWith(APlayerState* PlayerState) {
	Super::OverrideWith(PlayerState);
	if (PlayerState) {
		AMyPlayerState* TestPlayerState = Cast<AMyPlayerState>(PlayerState);
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OverrideWith"), *TestPlayerState->GetFName().ToString()));
		if (TestPlayerState) {
			S_PlayerInfo = TestPlayerState->S_PlayerInfo;
		}
	}
}