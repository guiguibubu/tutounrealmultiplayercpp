// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "PlayerInfo.h"
#include "MyPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPROJECTCPP_API AMyPlayerState : public APlayerState
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, Replicated)
	FPlayerInfo S_PlayerInfo;

	AMyPlayerState(const FObjectInitializer & ObjectInitializer);

	void CopyProperties(APlayerState* PlayerState) override;
	void OverrideWith(APlayerState* PlayerState) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
