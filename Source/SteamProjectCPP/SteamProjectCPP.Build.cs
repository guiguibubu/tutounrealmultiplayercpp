// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SteamProjectCPP : ModuleRules
{
	public SteamProjectCPP(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicLibraryPaths.AddRange(new string[] {
             "SteamProjectCPP"
         });

        PublicDependencyModuleNames.AddRange(
            new string[] {
                "Core",
                "CoreUObject",
                "Engine",
                "InputCore",
                "HeadMountedDisplay",
                "OnlineSubsystem",
                "OnlineSubsystemUtils",
                "UMG",
                "Slate",
                "SlateCore",
                "Sockets"
            });

        DynamicallyLoadedModuleNames.Add("OnlineSubsystemNull");
    }
}
