// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterSelectButton.h"

#include "Components/TextBlock.h"
#include "Components/Button.h"

UCharacterSelectButton::UCharacterSelectButton(const FObjectInitializer& ObjectInitializer) {
	
}

bool UCharacterSelectButton::Initialize() {
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(ButtonCharacter.IsValid())) return false;

	bool hasName = false;
	int nbChildren = ButtonCharacter->GetChildrenCount();
	for (int i = 0; i < nbChildren && !hasName; ++i) {
		TWeakObjectPtr<UWidget> child = ButtonCharacter->GetChildAt(i);
		hasName = child.IsValid() && child->IsA(UTextBlock::StaticClass());
		if (hasName) {
			TWeakObjectPtr<UTextBlock> childCasted = Cast<UTextBlock>(child);
			if (childCasted.IsValid()) {
				childCasted->SetText(nomCharacter_);
			}
		}
	}

	ButtonCharacter->OnClicked.AddDynamic(this, &UCharacterSelectButton::OnClickedCallback);

	FSlateBrush onNormal = ButtonCharacter->WidgetStyle.Normal;
	FSlateBrush onHovered = ButtonCharacter->WidgetStyle.Hovered;
	FSlateBrush onPressed = ButtonCharacter->WidgetStyle.Pressed;

	if (ensure(IsValid(image))) {
		onNormal.SetResourceObject(image);
		onHovered.SetResourceObject(image);
		onPressed.SetResourceObject(image);
	}

	onNormal.DrawAs = ESlateBrushDrawType::Image;
	onHovered.DrawAs = ESlateBrushDrawType::Image;
	onPressed.DrawAs = ESlateBrushDrawType::Image;

	onHovered.TintColor = FLinearColor{ 1.0, 0.918258, 0.164456, 1.0 };
	onPressed.TintColor = FLinearColor{ 1.0, 0.109751, 0.050025, 1.0 };

	ButtonCharacter->WidgetStyle.SetNormal(onNormal);
	ButtonCharacter->WidgetStyle.SetHovered(onHovered);
	ButtonCharacter->WidgetStyle.SetPressed(onPressed);

	return true;
}

void UCharacterSelectButton::OnClickedCallback() {
	OnClickedEvent.Broadcast(id_);
}