// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterSelectWidget.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "../Network/Lobby/LobbyPC.h"
#include "Engine/Engine.h"
#include "UI/CharacterSelectButtonPoolWidget.h"

UCharacterSelectWidget::UCharacterSelectWidget(const FObjectInitializer& ObjectInitializer) {
	
}

bool UCharacterSelectWidget::Initialize() {
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(Titre.IsValid())) return false;
	Titre->SetText(FText::FromString("Select Character"));

	if (!ensure(BackButton.IsValid())) return false;
	BackButton->OnClicked.AddDynamic(this, &UCharacterSelectWidget::HideWidget);

	if (!ensure(CharacterPanel.IsValid())) return false;
	CharacterPanel->OnClickedEvent.AddUObject(this, &UCharacterSelectWidget::CharacterSelected);
	
	if (!ensure(UnselectButton.IsValid())) return false;
	UnselectButton->OnClicked.AddDynamic(this, &UCharacterSelectWidget::UnselectCharacter);

	characterSelectedID = 0;

	return true;
}

void UCharacterSelectWidget::CharacterSelected(int16 id) {
	characterSelectedID = id;
	TWeakObjectPtr<ALobbyPC> playerController = Cast<ALobbyPC>(GetOwningPlayer());
	if (playerController.IsValid()) {
		TWeakObjectPtr<UTexture2D> image = (id > 0) ? characterImages[id-1] : nullptr;
		playerController->AssignSelectedCharacter(id, image.Get());
		HideWidget();
	}
}

void UCharacterSelectWidget::UnselectCharacter() {
	CharacterSelected(0);
}

void UCharacterSelectWidget::HideWidget() {
	Hide();
	Setup(true, EInputModeWidget::GameAndUI, false);
}
