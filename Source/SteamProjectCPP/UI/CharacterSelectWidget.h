// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "UI/CharacterSelectButtonPoolWidget.h"
#include "Engine/Texture2D.h"
#include "CharacterSelectWidget.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPROJECTCPP_API UCharacterSelectWidget : public UMenuWidget
{
	GENERATED_BODY()
public:
	UCharacterSelectWidget(const FObjectInitializer & ObjectInitializer);

	UPROPERTY(EditAnywhere, Category = "Variables")
	int16 characterSelectedID;
	UPROPERTY(EditAnywhere, Category = "Variables")
	TArray<UTexture2D*> characterImages;

	UFUNCTION()
	virtual bool Initialize();

	//void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<UCharacterSelectButtonPoolWidget> CharacterPanel;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> UnselectButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> BackButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UTextBlock> Titre;

	UFUNCTION()
	void CharacterSelected(int16 id);

	UFUNCTION()
	void UnselectCharacter();

	UFUNCTION()
	void HideWidget();
};
