// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "ConnectedPlayerWidget.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPROJECTCPP_API UConnectedPlayerWidget : public UMenuWidget
{
	GENERATED_BODY()
	
};
