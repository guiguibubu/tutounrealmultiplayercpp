// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "ErrorWidget.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPROJECTCPP_API UErrorWidget : public UMenuWidget
{
	GENERATED_BODY()
	
public:
	UErrorWidget(const FObjectInitializer & ObjectInitializer);
private:
	UFUNCTION()
	virtual bool Initialize();

	UPROPERTY(EditAnywhere, Category = "Variables")
	FText message;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> AcceptButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UTextBlock> MessageError;

	UPROPERTY()
	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

	UFUNCTION()
	void QuitGame();

	UFUNCTION()
	void MainMenu();

	UFUNCTION()
	void DestroySession();
};
