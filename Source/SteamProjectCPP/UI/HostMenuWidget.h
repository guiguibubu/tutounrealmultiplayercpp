// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
//#include "ScriptDelegates.h"
#include "HostMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPROJECTCPP_API UHostMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
	
public:
	UHostMenuWidget(const FObjectInitializer & ObjectInitializer);
private:
	UFUNCTION()
	virtual bool Initialize();

	bool enableAccept_ = false;
	bool sessionCreated_ = false;
	static const int NB_MIN_PLAYER = 2;
	static const int NB_MAX_PLAYER = 5;
	int nbPlayer_;

	FText nomSession_;

	class TScriptDelegate<> OnUserServerNameInputChange;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> AcceptButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> BackButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> IncreasePlayersButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> DecreasePlayersButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UEditableTextBox> UserServerNameInput;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UTextBlock> NbPlayerMessage;

	UPROPERTY()
	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

	UFUNCTION()
	void MainMenu();

	UFUNCTION()
	void LaunchServer();

	UFUNCTION()
	void IncreaseNbPlayer();
	
	UFUNCTION()
	void DecreaseNbPlayer();

	UFUNCTION()
	void NbPlayerModify(int newNbPlayer);

	UFUNCTION()
	void NameModify(FText name);

	UFUNCTION()
	void SessionCreated(bool sessionCreated);
};
