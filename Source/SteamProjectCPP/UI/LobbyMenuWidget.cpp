// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyMenuWidget.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "GameFramework/Pawn.h"

#include "Network/GameInfoInstance.h"

#include "Network/Lobby/LobbyPC.h"
#include "Network/Lobby/LobbyGM.h"

#include "UI/CharacterSelectWidget.h"

ULobbyMenuWidget::ULobbyMenuWidget(const FObjectInitializer & ObjectInitializer)
{
}

bool ULobbyMenuWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(StartButton.IsValid())) return false;
	StartButton->OnClicked.AddDynamic(this, &ULobbyMenuWidget::StartGame);
	StartButton->SetIsEnabled(false);
	StartButton->bIsEnabledDelegate.BindDynamic(this, &ULobbyMenuWidget::EnableStart);
	//int nbChildren = StartButton->GetChildrenCount();

	if (!ensure(ChooseCharacterButton.IsValid())) return false;
	ChooseCharacterButton->OnClicked.AddDynamic(this, &ULobbyMenuWidget::ChooseCharacter);

	if (!ensure(QuitButton.IsValid())) return false;
	QuitButton->OnClicked.AddDynamic(this, &ULobbyMenuWidget::MainMenu);

	gameInstanceRef = GetGameInstance<UGameInfoInstance>();

	CharacterSelectWD = Cast<UCharacterSelectWidget>(gameInstanceRef->createWidget(GetOwningPlayer(), gameInstanceRef->CharacterSelectWD_Class));

	return true;
}

void ULobbyMenuWidget::StartGame() {
	TWeakObjectPtr<UWorld> world = GetWorld();
	if (!ensure(world.IsValid())) return;
	if (world->IsServer()) {
		this->Clean();
		TWeakObjectPtr<ALobbyGM> gameMode = world->GetAuthGameMode<ALobbyGM>();
		if (gameMode.IsValid()) {
			gameMode->StartGame();
		}
	}
}

void ULobbyMenuWidget::ChooseCharacter() {
	if (!CharacterSelectWD.IsValid()) {
		
	}
	TWeakObjectPtr<UMenuWidget> widgetCast = Cast<UMenuWidget>(CharacterSelectWD);
	gameInstanceRef->showWidget(widgetCast, gameInstanceRef->CharacterSelectWD_Class, GetOwningPlayer(), true);
}

void ULobbyMenuWidget::MainMenu() {
	this->Clean();
	TWeakObjectPtr<APlayerController> playerController = GetOwningPlayer();
	if (!ensure(playerController.IsValid())) return;
	TWeakObjectPtr<ALobbyPC> playerControllerCast = Cast<ALobbyPC>(playerController);
	if (!ensure(playerControllerCast.IsValid())) return;
	playerControllerCast->DestroySession();
	//gameInstanceRef->mainMenu(playerController.Get());
}

bool ULobbyMenuWidget::EnableStart() {
	bool enableStart = false;
	TWeakObjectPtr<UWorld> world = GetWorld();
	TWeakObjectPtr<ALobbyGM> gameMode = Cast<ALobbyGM>(world->GetAuthGameMode());
	if (world->IsServer()) {
		if (gameMode.IsValid()) { 
			enableStart = gameMode->canWeStart; 
		}
	}
	else {
		enableStart = CharacterSelectWD.IsValid() && CharacterSelectWD->characterSelectedID > 0;
	}
	return enableStart;
}