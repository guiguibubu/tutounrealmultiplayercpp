// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "UI/CharacterSelectWidget.h"
#include "Network/GameInfoInstance.h"
#include "LobbyMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class STEAMPROJECTCPP_API ULobbyMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
public:
	ULobbyMenuWidget(const FObjectInitializer & ObjectInitializer);

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> StartButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> ChooseCharacterButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> QuitButton;

	UPROPERTY()
	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

	UPROPERTY()
	TWeakObjectPtr<UCharacterSelectWidget> CharacterSelectWD;

	UFUNCTION()
	virtual bool Initialize();

	UFUNCTION()
	void StartGame();

	UFUNCTION()
	void ChooseCharacter();

	UFUNCTION()
	void MainMenu();

	UFUNCTION()
	bool EnableStart();
};
